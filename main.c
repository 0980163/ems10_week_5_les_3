#include <msp430.h>
#include "inc/bibliotheek.h"

/**
 * main.c
 */
int main(void)
{
    WDTCTL = WDTPW | WDTHOLD; // stop de watchdog timer

    // initialiseer de klok op 1 MHz
    // bekijk of false wordt teruggegeven
    if (zet_klok_op_MHz(1) == false)
    {
        while (1); // blijf hier hangen zodat dit wordt opgemerkt met het debuggen
    }

    // push button op P1.0 met interne pull-down
    zet_pin_richting(1, 0, input);
    zet_pin_richting(1, 1, input);

    zet_interne_weerstand(1, 0, pull_down);
    zet_interne_weerstand(1, 1, pull_down);

    // led op P2.1
    zet_pin_richting(2, 0, output);
    zet_pin_richting(2, 1, output);
    zet_pin_richting(2, 2, output);
    output_pin(2, 0, laag);
    output_pin(2, 1, laag);
    output_pin(2, 2, laag);

    int fout = 0;

    __delay_cycles(3000);

    while (1)
    {
        int toestand = 0, tikken = 0, c_1 = 0, c_2 = 0, c_3 = 0, c_4 = 0;
        if (toestand == 0) {
            output_pin(2, 0, hoog);
            output_pin(2, 2, laag);
        }

        while (input_pin(1, 0) == false) {
            if (input_pin(1, 1)) {
                c_1 = c_1 + 1;
                while (input_pin(1, 1)) {}
            }
        }
        while (input_pin(1, 0)) {}

        while (input_pin(1, 0) == false) {
            if (input_pin(1, 1)) {
                c_2 = c_2 + 1;
                while (input_pin(1, 1)) {}
            }
        }
        while (input_pin(1, 0)) {}

        while (input_pin(1, 0) == false) {
            if (input_pin(1, 1)) {
                c_3 = c_3 + 1;
                while (input_pin(1, 1)) {}
            }
        }
        while (input_pin(1, 0)) {}

        while (input_pin(1, 0) == false) {
            if (input_pin(1, 1)) {
                c_4 = c_4 + 1;
                while (input_pin(1, 1)) {}
            }
        }
        while (input_pin(1, 0)) {}

        output_pin(2, 0, laag);
        __delay_cycles(1000000);

        if (c_1 == 0 && c_2 == 1 && c_3 == 6 && c_4 == 3) {
            toestand = 1;
        }

        switch (toestand) {
        case 0:
            output_pin(2, 0, hoog);
            break;
        case 1:
            while (1) {
            output_pin(2, 2, hoog);
            // als de code juist is ingevoerd dan gaat de blauwe led aan
        }
        }

        fout = fout + 1;

        if (fout == 3) {
            while (tikken < 300) {
                output_pin(2, 0, hoog);
                __delay_cycles(500000);
                output_pin(2, 0, laag);
                __delay_cycles(500000);
                tikken = tikken + 1;
                // als de code drie keer op rij onjuist is ingevoerd dan gaat de rode led 5 minuten lang knipperen
            }
            fout = 0;
        }
    }
}
